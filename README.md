# Docker Compose Integration Test

This is the code to accompany the blog post at [http://blog.dupplaw.me.uk/articles/2018-08/integration-test-with-docker](http://blog.dupplaw.me.uk/articles/2018-08/integration-test-with-docker).

## Building

This is a Java JUnit test that runs an integration test against a service.

To build it use:

```
docker build -t integration-test .
```

This will build the docker container ready for you to continue with the blog 
post :)