package uk.me.dupplaw.david.integration;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import javax.jms.*;
import java.util.ArrayList;
import java.util.List;

public class ActiveMQRule implements TestRule {

    private static class MessageHandler implements MessageListener {
        private final ActiveMQRule rule;

        MessageHandler(ActiveMQRule rule) {
            this.rule = rule;
        }

        @Override
        public void onMessage(Message message) {
            System.out.println("Received message...");
            rule.newMessage(message);
        }
    }

    private String outputQueueName;
    private String inputQueueName;
    private String hostName ="localhost";

    private Queue destination;
    private MessageProducer producer;
    private List<Message> messages = new ArrayList<>();

    public ActiveMQRule onHost(String hostName) {
        this.hostName = hostName;

        if( this.hostName == null ) {
            this.hostName = "localhost";
        }

        return this;
    }

    public ActiveMQRule withOutputQueue(String outputQueueName) {
        this.outputQueueName = outputQueueName;
        return this;
    }

    public ActiveMQRule withInputQueue(String inputQueueName) {
        this.inputQueueName = inputQueueName;
        return this;
    }

    public ActiveMQRule sendMessage(Message message) {
        try {
            System.out.println("Sending message to " + destination);
            producer.send(message);
        } catch (JMSException e) {
            e.printStackTrace();
        }
        return this;
    }

    @Override
    public Statement apply(Statement base, Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
                        "failover://tcp://"+hostName+":61616");
                final Connection connection = connectionFactory.createConnection();

                System.out.println("Connecting to ActiveMQ at tcp://"+hostName+":61616 ...");
                connection.start();

                Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                destination = session.createQueue(outputQueueName);
                producer = session.createProducer(destination);

                Queue source = session.createQueue(inputQueueName);
                MessageConsumer consumer = session.createConsumer(source);
                consumer.setMessageListener(new MessageHandler(ActiveMQRule.this));

                messages.clear();

                base.evaluate();

                connection.close();
            }
        };
    }

    public ActiveMQRule waitForMessage(int numberOfMillsToWait) {
        try {
            long start = System.currentTimeMillis();
            while (messages.size() == 0 && (System.currentTimeMillis() - start) < numberOfMillsToWait) {
                Thread.sleep(50);
            }

            if (messages.size() == 0) {
                throw new AssertionError("Timed out - no messages received within " + numberOfMillsToWait + "ms");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return this;
    }

    private void newMessage(Message message) {
        messages.add(message);
    }

    public int numberOfMessagesReceived() {
        return messages.size();
    }

    public Message getMessage(int i) {
        return messages.get(i);
    }
}
