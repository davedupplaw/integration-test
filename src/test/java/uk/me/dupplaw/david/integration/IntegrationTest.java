package uk.me.dupplaw.david.integration;

import org.apache.activemq.command.ActiveMQMapMessage;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.junit.Rule;
import org.junit.Test;

import javax.jms.JMSException;

import static org.assertj.core.api.Assertions.assertThat;

public class IntegrationTest {
    @Rule public ActiveMQRule activeMQClient = new ActiveMQRule()
                                                        .onHost(System.getenv("ACTIVEMQ_HOST"))
                                                        .withOutputQueue("input-queue")
                                                        .withInputQueue("output-queue");

    @Test
    public void shouldReceiveMessageOnOutputQueueWhenMessageSentOnInputQueue() throws JMSException {
        ActiveMQMapMessage message = new ActiveMQMapMessage();
        message.setString("message", "Hello world!");

        activeMQClient.sendMessage(message).waitForMessage(500 );

        assertThat(activeMQClient.numberOfMessagesReceived() ).isEqualTo(1);
        assertThat(activeMQClient.getMessage(0) ).isInstanceOf(ActiveMQTextMessage.class );

        ActiveMQTextMessage textMessage = (ActiveMQTextMessage) activeMQClient.getMessage(0);
        assertThat( textMessage.getText() ).isEqualTo("Message was: Hello world!");
    }
}
